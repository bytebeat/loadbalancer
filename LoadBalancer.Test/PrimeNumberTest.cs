using LoadBalancer.Avalonia.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoadBalancer.Test
{
    [TestClass]
    public class PrimeNumberTest
    {
        [TestMethod]
        public void TestIsPrime()
        {
            var expected = true;
            // 123 = 3 x 41
            long primeCandidate = 123;
            var result = primeCandidate.IsPrime();
            Assert.AreEqual(expected, result);
        }
    }
}
