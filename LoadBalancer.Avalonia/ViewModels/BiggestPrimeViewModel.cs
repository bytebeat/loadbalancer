﻿using Avalonia.Controls;
using Avalonia.Threading;
using LoadBalancer.Avalonia.Models;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace LoadBalancer.Avalonia.ViewModels
{
    public class BiggestPrimeViewModel : ViewModelBase
    {
        private bool IsCalculating;
        private long PrimeCandidate;
        private readonly CancellationTokenSource Cts;

        internal Button StartStopButton { get; set; } = new Button();
        internal TextBlock BigPrimeTextBlock { get; set; } = new TextBlock();
        internal TextBlock ElapsedTextBlock { get; set; } = new TextBlock();

        public BiggestPrimeViewModel()
        {
            Cts = new CancellationTokenSource();
        }

        internal void StartOrStop()
        {
            if (IsCalculating)
            {
                IsCalculating = false;
                StartStopButton.Content = "Resume";
            }
            else
            {
                IsCalculating = true;
                StartStopButton.Content = "Stop";

                // https://docs.microsoft.com/en-us/dotnet/api/system.windows.threading.dispatcher?view=netframework-4.8
                Dispatcher.UIThread.InvokeAsync(CheckNextNumber, DispatcherPriority.Normal);
            }
        }

        internal async void CheckNextNumber()
        {
            // Workaround for unexpected Dispatcher behaviour with Avalonia.
            // https://github.com/AvaloniaUI/Avalonia/issues/5018
            //await Task.Yield();
            await Task.Delay(100);

            var x = new Stopwatch();
            x.Start();

            if (PrimeCandidate.IsPrime())
            {
                x.Stop();
                BigPrimeTextBlock.Text = PrimeCandidate.ToString();
                ElapsedTextBlock.Text = x.ElapsedTicks.ToString();
            }

            PrimeCandidate += 2;
            if (IsCalculating)
            {
                await Dispatcher.UIThread.InvokeAsync(CheckNextNumber, DispatcherPriority.SystemIdle);

                ProgressBar[] jobServers =
                {
                    ServerLoadViewModel.JobServer1,
                    ServerLoadViewModel.JobServer2,
                    ServerLoadViewModel.JobServer3,
                    ServerLoadViewModel.JobServer4
                };
                await ServerLoadViewModel.EnqueueJobAsync(jobServers, Cts.Token);
                // 5s delay = Job done and ready to dequeue.
                await Task.Delay(5000);
                await ServerLoadViewModel.DequeueJobAsync(jobServers, Cts.Token);
            }
        }
    }
}
