﻿using Avalonia.Controls;
using Avalonia.Threading;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LoadBalancer.Avalonia.ViewModels
{
    public class ServerLoadViewModel : ViewModelBase
    {
        // Must be static because of the static method LoadProgressAsync() called from:
        // BiggestPrimeViewModel.CheckNextNumber()
        internal static ProgressBar JobServer1 { get; set; } = new ProgressBar();
        internal static ProgressBar JobServer2 { get; set; } = new ProgressBar();
        internal static ProgressBar JobServer3 { get; set; } = new ProgressBar();
        internal static ProgressBar JobServer4 { get; set; } = new ProgressBar();

        internal static async Task DequeueJobAsync(ProgressBar[] jobServers, CancellationToken cancellationToken = default)
        {
            var task = Task.Factory.StartNew(() =>
            {
                Dispatcher.UIThread.InvokeAsync(() =>
                {
                    // https://stackoverflow.com/questions/108819/best-way-to-randomize-an-array-with-net
                    Random rnd = new Random();
                    ProgressBar[] randomizedJobServerCollection = jobServers.OrderBy(x => rnd.Next()).ToArray();

                    Progress<int> progress = new Progress<int>();
                    int jobServerServerLoadValue;

                    // Iterate over randomized "Servers".
                    foreach (var jobServerProgressBar in randomizedJobServerCollection)
                    {
                        // Currently "working".
                        if (jobServerProgressBar.Value > 0)
                        {
                            progress = new Progress<int>(value => jobServerProgressBar.Value = value);
                            jobServerProgressBar.Value--;
                            jobServerServerLoadValue = (int)jobServerProgressBar.Value;
                            ((IProgress<int>)progress).Report(jobServerServerLoadValue);
                            break;
                        }
                    }

                    if (cancellationToken.IsCancellationRequested)
                        return;
                });
            }, cancellationToken);
            await task;
        }

        internal static async Task EnqueueJobAsync(ProgressBar[] jobServers, CancellationToken cancellationToken = default)
        {
            var task = Task.Factory.StartNew(() =>
            {
                Dispatcher.UIThread.InvokeAsync(() =>
                {
                    // https://stackoverflow.com/questions/108819/best-way-to-randomize-an-array-with-net
                    Random rnd = new Random();
                    ProgressBar[] randomizedJobServerCollection = jobServers.OrderBy(x => rnd.Next()).ToArray();

                    Progress<int> progress = new Progress<int>();
                    int jobServerServerLoadValue;

                    // Iterate over randomized "Servers".
                    foreach (var jobServerProgressBar in randomizedJobServerCollection)
                    {
                        // "Capacity" below 50 %.
                        if (jobServerProgressBar.Value < 50)
                        {
                            progress = new Progress<int>(value => jobServerProgressBar.Value = value);
                            jobServerProgressBar.Value++;
                            jobServerServerLoadValue = (int)jobServerProgressBar.Value;
                            ((IProgress<int>)progress).Report(jobServerServerLoadValue);
                            break;
                        }

                        // "Capacity" below 50 %.
                        if (jobServerProgressBar.Value < 99)
                        {
                            progress = new Progress<int>(value => jobServerProgressBar.Value = value);
                            jobServerProgressBar.Value++;
                            jobServerServerLoadValue = (int)jobServerProgressBar.Value;
                            ((IProgress<int>)progress).Report(jobServerServerLoadValue);
                            break;
                        }

                        // "Pause" if no "Server" has free "Capacity".
                        return;
                    }

                    if (cancellationToken.IsCancellationRequested)
                        return;
                });
            }, cancellationToken);
            await task;
        }
    }
}
