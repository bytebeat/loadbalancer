﻿using System;

namespace LoadBalancer.Avalonia.Models
{
    public static class PrimeNumber
    {
        public static bool IsPrime(this long primeCandidate)
        {
            for (long i = 3; i <= Math.Sqrt(primeCandidate); i++)
            {
                if (primeCandidate % i != 0)
                    return true;
            }
            return false;
        }
    }
}
