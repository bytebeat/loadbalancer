using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LoadBalancer.Avalonia.ViewModels;

namespace LoadBalancer.Avalonia.Views
{
    public class BiggestPrime : UserControl
    {
        public BiggestPrime()
        {
            InitializeComponent();

            DataContext = new BiggestPrimeViewModel
            {
                StartStopButton = this.FindControl<Button>("startStopButton"),
                BigPrimeTextBlock = this.FindControl<TextBlock>("bigPrime"),
                ElapsedTextBlock = this.FindControl<TextBlock>("elapsed")
            };
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
