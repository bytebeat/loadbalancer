## Introduction

This project was founded to practice (object-oriented) programming in spare time.

## Resources

- .NET 5.0
- Avalonia UI Framework

## What's contained in this project

It contains a crossplattform WPF-application (simulation) that calculates prime numbers beginning at 0,
displaying the application user the currrent biggest prime number; the calculation is done asynchronous on 4 "JobServers".
The "JobServers" enqueue a new Job (calculate a prime number) every one-tenth of a second and are allowed to dequeue after five seconds.
The result is a constant increasing/decreasing progress on four different "JobServer" to demonstrate the load balancing in the background.